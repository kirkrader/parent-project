#!/bin/bash

# Invoke the latest deployed version of
# us.rader:parent-project-archetype

# Usage: new-project.sh -h | --help | [mavenCoords] [package]

# mvnCoords are in the form [groupId:]artifactId[:version]

# groupId defaults to us.rader.playform
# artifactId defaults to demo-parent
# version defaults to 1.0-SNAPSHOT

# note that if not supplied, the package property passed to the
# artifact will be the same as groupId so this must conform to the
# syntax for a Java package name (which is the default Maven
# convention)

function usage {

    echo 'usage: new-project.sh [maven-coords [package]]'
    echo 'where maven-coords = (artifactId | groupId:artifactId | groupId:artifactId:version)'

}

GROUPID=us.rader.playform
ARTIFACTID=demo-parent
VERSION=1.0-SNAPSHOT

if [[ $# -eq 1 && ( $1 == '-h' || $1 == '--help' ) ]]; then

    usage
    exit 0

fi

if [[ $# -gt 2 ]]; then

    usage
    exit 10

fi

if [[ $# -gt 0 ]]; then

    IFS=':' read -ra COORDS <<< "$1"

    if [[ ${#COORDS[@]} -gt 2 ]]; then

        VERSION=${COORDS[2]}

    fi

    if [[ ${#COORDS[@]} -gt 1 ]]; then

        GROUPID=${COORDS[0]}
        ARTIFACTID=${COORDS[1]}

    else

        ARTIFACTID=${COORDS[0]}

    fi

fi

if [[ $# -gt 1 ]]; then

    PACKAGE=$2

else

    PACKAGE=$GROUPID

fi

mvn archetype:generate \
    -DarchetypeGroupId=us.rader \
    -DarchetypeArtifactId=parent-project-archetype \
    -DarchetypeVersion=1.0-SNAPSHOT \
    -DgroupId=$GROUPID \
    -DartifactId=$ARTIFACTID \
    -Dversion=$VERSION \
    -Dpackage=$PACKAGE \
    -DinteractiveMode=false
