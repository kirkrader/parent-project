#!/bin/bash

# Demonstrate the basic workflow for using parent-project-archetype
# and child-project-archetype. Also serves as ad hoc acceptance tests
# for these archetypes.

# This script assumes that Java 8 or later and Maven 3.3.9 or later
# are installed and properly configured on your machine for use with
# our overall build environment, including the proper contents of the
# ~/.m2/settings.xml file as described in Confluence.

# This script also assumes that
# us.rader:parent-project-archetype:1.0-SNAPSHOT and
# us.rader:child-project-archetype:1.0-SNAPSHOT are properly
# installed.

# Finally, this script assumes that all of the prerequisites for
# projects generated using those archetypes are met by your build
# environment.

# NOTE: There are issues with a number of the Maven plugins and
# build-time dependencies such that the 'mvn site' phase emits many
# Java exception stack traces and dire-looking warning messages to the
# console. These do not constitute a broken build.

echo --------
echo Clean...

RUNNING=$(docker inspect --format '{{.State.Running}}' new-service 2> /dev/null)

if [ "$RUNNING" == "true" ]; then

    echo Stopping new-service container...
    docker stop new-service

fi

docker inspect new-service &> /dev/null

if [ $? -eq 0 ]; then

    echo Deleting new-service container
    docker rm new-service

fi

docker inspect parasaurolophus/new-service &> /dev/null

if [ $? -eq 0 ]; then

    echo Deleting parasaurolophus/new-service image
    docker rmi parasaurolophus/new-service

fi

if [ -d demo-parent ]; then

    echo Deleting demo-parent directory
    rm -rf demo-parent

fi

if [ -f demo-parent ]; then

    echo demo-parent exists!
    exit 10

fi

echo ---------------------------------------
echo Generate parent project using archetype

mvn archetype:generate \
    -DarchetypeGroupId=us.rader \
    -DarchetypeArtifactId=parent-project-archetype \
    -DarchetypeVersion=1.0-SNAPSHOT \
    -DgroupId=us.rader.demo \
    -DartifactId=demo-parent \
    -Dversion=1.0-SNAPSHOT \
    -Dpackage=us.rader.demo \
    -DinteractiveMode=false

if [ $? -ne 0 ]; then

    echo Archetype failed
    exit 20

fi

cd demo-parent

echo -------------------------------------
echo Generate child module using archetype

mvn archetype:generate \
    -DarchetypeGroupId=us.rader \
    -DarchetypeArtifactId=child-project-archetype \
    -DarchetypeVersion=1.0-SNAPSHOT \
    -DartifactId=new-service \
    -Dpackage=us.rader.newservice \
    -DinteractiveMode=false

if [ $? -ne 0 ]; then

    echo Archetype failed
    exit 30

fi

echo -------------------------------------
echo Build modules and run automated tests

mvn clean install -DDO_INTEGRATION_TESTS

if [ $? -ne 0 ]; then

    echo Build failed
    exit 40

fi

echo -------------------
echo Build documentation

mvn site site:stage

if [ $? -ne 0 ]; then

    echo Documentation build failed
    exit 50

fi

echo ------------------
echo Build Docker image
pushd new-service
mvn docker:build

if [ $? -ne 0 ]; then

    echo Docker build failed
    exit 60

fi

popd

echo -----------------------
echo Create Docker container

docker create -p 127.0.0.1:8081:8080 --name new-service parasaurolophus/new-service

if [ $? -ne 0 ]; then

    echo Docker container creation failed
    exit 70

fi

echo ----------------------
echo Start Docker container

docker start new-service

if [ $? -ne 0 ]; then

    echo Starting Docker container failed
    exit 80

fi

echo --------------------------------------------
echo Delay a bit to give service time to start...

sleep 5

echo ----------------------------------
echo Call service discovery endpoint...

curl http://localhost:8081/swagger-resources

if [ $? -eq 0 ]; then

    echo ""

else

    echo Error getting Swagger resources
    exit 90

fi

echo --------------------------------
echo Call new-service service endpoint...

RESPONSE=$(curl http://localhost:8081/new-service/1 2> /dev/null)
echo http://localhost:8081/new-service/1 returned $RESPONSE

echo ------------------------
echo Stop Docker container...

docker stop new-service
