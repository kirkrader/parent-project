Copyright &copy; 2017 Kirk Rader

![kirk](src/main/resources/archetype-resources/logo.png)

# Parent Project Archetype

Maven archetype that creates our standard project layout with
pre-configured POM files. See also
*us.rader:child-project-archetype* for generating additional modules
in a parent project created using this one.

## Features

Features of projects created using this archetype include:

- Parent POM that includes our base dependencies and run time
  environment assumptions

- Profiles for building on Linux (preferred), Mac OS X and Windows

- *build-tools* module that configures our standard code-quality rules
  and filters for checkstyle and FindBugs

- *demo-service* module that contains a "hello world" Spring Boot
  application

- The *demo-service* module incorporates Swagger such that the
  resulting service is discoverable and self-describing as per the
  Open API specification -- <https://www.openapis.org>

The generated POM files support unit and integration tests using the
Maven *surefire* and *failsafe* plugins, respectively. They also
support creating a Docker image to containerize the Spring Boot
application.

# Build

```bash
git clone https://gitlab.com/parasaurolophus/parent-project-archetype.git
cd parent-project-archetype
mvn install
qmvn archetype:crawl
```

# Usage

First confirm that Java 8 or later and Maven 3.3.9 or later are installed on
your machine.

Then, to generate a new project interactively using this archetype:

    mvn archetype:generate \
        -DarchetypeGroupId=us.rader \
        -DarchetypeArtifactId=parent-project-archetype

You can also fully specify the generated project's *groupId*, *artifactId* etc.
using additional *-D* options as with any Maven archetype.

Either way, this will result in a new Maven project with the following
structure:

    -+ pom.xml
     |
     + README.md
     |
     + logo.png
     |
     + Doxyfile
     |
     + build-tools -+ pom.xml -+ ...
     |
     + demo-service -+ pom.xml
                     |
                     + src -+ main -+ ...
                            |
                            + test -+ ...
                            |
                            + it -+ ...

As described above, the *demo-service/main*,
*demo-service/test* and *demo-service/it* directories will
be pre-populated with a simple "Hello world" web service,
unit tests and integration tests, respectively.

The *build-tools* project contains shared build-time
configuration used by all other modules in the parent
project.

See the *README.md* in the generated project for more
details.
