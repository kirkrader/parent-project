#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
#set( $dt = $symbol_pound.getClass().forName("java.util.Date").newInstance() )
#set( $year = $dt.getYear() + 1900 )
/*
 * Copyright ${year} Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ${package};

import io.swagger.annotations.Api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 'Hello world!' service.
 */
@Api(tags = { "${artifactId}" })
@RestController
@RequestMapping("/${artifactId}")
@EnableAutoConfiguration
@CrossOrigin(origins = "*") // TODO change this for production
public final class DemoService {

    /**
     * Application command-line arguments.
     */
    private final String[] args;

    /**
     * Constructor that obtains the original application command-line arguments.
     *
     * @param applicationArgs
     *            ApplicationArguments instance
     */
    @Autowired
    public DemoService(final ApplicationArguments applicationArgs) {

        args = applicationArgs.getSourceArgs();

    }

    /**
     * End-point for ${artifactId} API.
     *
     * @return The string, "Hello from ${artifactId}"
     */
    @GetMapping
    String get() {

        return "Hello from ${artifactId}";

    }

    /**
     * Return the specified container process command-line argument.
     *
     * @param arg
     *            The argument index.
     *
     * @return The specified argument's value.
     */
    @GetMapping("/{arg}")
    ResponseEntity<GetArgResult> getArg(@PathVariable("arg") final int arg) {

        if (arg >= 0 && arg < args.length) {

            return ResponseEntity.ok(new GetArgResult(arg, args[arg]));

        }

        return ResponseEntity.badRequest().body(null);

    }
}
