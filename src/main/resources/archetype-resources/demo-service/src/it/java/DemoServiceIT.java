#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
#set( $dt = $symbol_pound.getClass().forName("java.util.Date").newInstance() )
#set( $year = $dt.getYear() + 1900 )
/*
 * Copyright ${year} Kirk Rader
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ${package};

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

import java.net.URL;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Integration tests for {@link DemoService}.
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
        classes = { DemoService.class })
public final class DemoServiceIT {

    /**
     * TCP/IP port on which server instance is run.
     */
    @LocalServerPort
    private int port;

    /**
     * Base URL for servlet instance.
     */
    private URL base;

    /**
     * REST template for integration tests.
     */
    @ Autowired
    private TestRestTemplate template;

    /**
     * Verify that a GET request to the DemoService end-point returns successfully.
     *
     * @throws Exception
     *             Thrown if an unexpected error occurs.
     */
    @Test
    public void getArg() throws Exception {

        final ResponseEntity<GetArgResult> response = template
                .getForEntity(base.toString() + "/1", GetArgResult.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.BAD_REQUEST));

    }

    /**
     * Verify that a GET request to the DemoService end-point returns successfully.
     *
     * @throws Exception
     *             Thrown if an unexpected error occurs.
     */
    @Test
    public void getHello() throws Exception {

        final ResponseEntity<String> response = template
                .getForEntity(base.toString(), String.class);
        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
        assertThat(response.getBody(), equalTo("Hello from ${artifactId}"));

    }

    /**
     * Initialize {@link #base}.
     *
     * @throws Exception
     *             Thrown if an unexpected error occurs.
     */
    @Before
    public void setUp() throws Exception {

        base = new URL("http://localhost:" + port + "/${artifactId}");

    }
}
