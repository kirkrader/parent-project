#set( $d = '$' )
#set( $second = '##' )
#set( $third = '###' )
#set( $fourth = '####' )
#set( $dt = $second.getClass().forName("java.util.Date").newInstance() )
#set( $year = $dt.getYear() + 1900 )

Copyright &copy; ${year} Kirk Rader

![kirk](logo.png)

# ${groupId}:${artifactId}

Stub project created by *us.rader:parent-project-archetype*

$second Supported Platforms

Ubuntu 16.04 is the preferred operating system for build servers and development
work stations. Use the native APT package manager to install the prerequisites
listed below.

Ubuntu 14.04 can be used so long as you are careful to select the correct
version and installation location for all of the packages listed below. In some
cases this requires bypassing the package manager and following manual
installation instructions, instead.

Similarly, other Linux distributions can be used if compatible versions of all
the following prerequisites can be installed. Again, using the native package
may select an inappropriate version or installation location. In such cases you
may need to use a manual installation procedure for some of the prerequisites
packages.

Any Macintosh running OS X from 10.11 or later can be easily configured using
[Homebrew](http://brew.sh/).

Using Windows is possible, but more complex to configure and with more
trade-offs depending on a given user's preferences and the exact version of the
OS.

Native packages exist for all of the following prerequisites for Windows 8 but
each must be manually installed and configured due to the lack of a package
manager.

At the time of this writing, not all of the prerequisites have native packages
for Windows 10, but it is possible to use the [Linux Subsystem for
Windows](https://msdn.microsoft.com/en-us/commandline/wsl) or
[Cygwin](https://www.cygwin.com/) and treat any version of Windows as if it were
a flavor of Linux for a user who is comfortable using command-line tools.

Similarly, if a given Mac or Windows based work station has sufficient hardware
capabilities it is possible to run an Ubuntu based development environment in a
[VM](https://www.virtualbox.org) for maximum compatibility with the build
server.

$second Prerequisites

The generated POM files assume certain tools and configuration settings are
installed in the environment from which Maven will be invoked.

$third Docker Engine

See <https://docs.docker.com> for information on installing and configuring
Docker Engine for your build environment.

$third Doxygen

This project's POM invokes [Doxygen](http://www.doxygen.org) using the
*org.codehaus.mojo:exec-maven-plugin*. You must install version Doxygen 1.8.8 or
later.

$fourth Mac

This project's POM assumes that [Doxygen](http://www.doxygen.org) is installed
at */usr/local/bin/doxygen*.

This can be accomplished on a Mac with [Homebrew](http://brew.sh/) installed
simply by executing:

    brew update
    brew install doxygen

$fourth Linux

The situation for Linux is more complex. Depending on the distrubution and
version, you are likely to get an incompatible version of Doxygen or have it
installed at the wrong path using the native package manager. For this reason,
you should always follow the instructions in the *Installing the binaries on
UNIX* section of <http://www.stack.nl/~dimitri/doxygen/manual/install.html> to
install the latest version of Doxygen on Linux. In particular, make sure that
you use the default installation prefix of */usr/local* and are installing
version 1.8.8 or later.

$fourth Windows

See <https://www.stack.nl/~dimitri/doxygen/download.html> for Windows installer.
Run with administrator privilege and choose default installation location and
configuration. See the Windows profile in the parent POM for the standard
Windows installation location.

$third Graphviz

Doxygen assumes that [Graphviz](http://www.graphviz.org/) is installed at the
default location for the given platform.

$fourth Mac

On a Mac this is again easily achieved using Homebrew:

    brew update
    brew install graphviz

$fourth Linux

Use the native package manager to install Graphviz on Linux. For example, on
Ubuntu 16.04 or later:

    sudo apt update
    sudo apt -y install graphviz

(Use *apt-get* instead of *apt* on Ubuntu 14.04 or earlier.)

$fourth Windows

Follow the instructions at <http://www.graphviz.org/Download.php> to download
and install Graphviz. You should also add the directory containing the Graphviz
*.exe* files to your *%PATH%* environment variable.

$third TeX Live (Optional)

Doxygen will have already pulled in some parts of the [TeX
Live](https://www.tug.org/texlive/) package as dependencies. If you wish to
generate PDF files or include rich LaTeX content in generated documentation you
should install the full *TeX Live* package or its equivalent for your platform.

$fourth Mac

Follow the instructions at <https://www.tug.org/mactex/>.

$fourth Linux

On Ubuntu 16.04 or later:

    sudo apt update
    sudo apt install -y texlive-full

As always, on earlier versions of Ubuntu substitute *apt-get* for *apt*.

Use your distribution's native package manager or follow the instructions at
<https://www.tug.org/texlive/quickinstall.html> for other flavors of Linux.

$fourth Windows

For Windows 8 or earlier, use [MiKTeX](https://miktex.org/). Ignore the instructions at <https://www.tug.org/texlive/windows.html>.

No, really.

At the time of this writing the current MiKTeX installer appears not to work on
Windows 10. That said, it is probably better to follow the instructions for
Linux using the *Linux Subsystem for Linux* ('bash for Windows') for all
prerequisites, including TeX Live, when using Windows 10, as described above.

$third PlantUML (Optional)

With [PlantUML](http://plantuml.com/) installed, you can embed UML diagrams
directly in comment blocks and they will be rendered in place by Doxygen. To do
this, download *plantuml.jar* from <http://plantuml.com/download> and set the
${d}PLANTUML_JAR environment variable to its absolute path.

> Note that because it is simply a Java .jar file, PlantUML is not supported by
> most platforms' native package managers.

$second Usage

> Note that all of the code examples here assume the use of *bash*, all
> prerequisites are installed in their standard locations and environment
> configuration is as described above. If you use native packages in Windows (as
> opposed to Cygwin or the Linux Subsystem for Windows) then you will have to
> adjust the shell and file system path syntax accordingly.

With all of the preceding prerequisites in place, use the following command to
build locally, running unit tests and basic code quality checks:

    mvn clean install

To run integration tests, invoke:

    mvn verify -DDO_INTEGRATION_TESTS

To generate documentation from the source code using Doygen:

    mvn site

Along with all the other output normally generated by the Maven *site* phase,
the Doxygen HTML output will then be in *target/site/doxygen/html* and the LaTeX
output in *target/site/doxygen/latex*. If you have the *texlive* package
installed, you can then generate a PDF using:

    pushd target/site/doxygen/latex
    make
    popd

The PDF version of the documentation generated using Doxygen will then be at
*target/site/doxygen/latex/refman.pdf* assuming that all prerequisites listed
above are installed, including *TeX Live*.

To generate a complete, consolidated set of build reports with all internal,
relative links properly resolved and assuming that you have already generated
the individual modules' reports using `mvn site`:

    mvn site:stage

See the "Related Pages" section of the documentation generated using Doxygen for
details of the example code generated by the archetype for this project.

To create and run a Docker image for the Spring Boot application in the
*demo-service* module:

    pushd demo-service
    mvn docker:build
    popd
    docker run -p 8080:8080 --name demo-service ${docker.image.prefix}/demo-service

$second Next Steps

You are now ready to start developing code in your new poject.

You probably will want to just delete the *demo-service* module, replacing it
with new modules by invoking *us.rader:child-project-archetype* in the parent
project directory. Do not, however, delete the *build-tools* module! See the
comments in the parent and module POM files for more information.

$third IDE

The *bash* commands in the preceding section correspond to various tasks carried
out during the CI/CD build process. The POM files generated by these archetypes
do not mandate nor directly support any particular IDE, though of course
developers are strongly encouraged to use IDE's to facilitate day-to-day coding
and testing tasks.

There is no single "best" way to import projects generated using these or any
other Maven archetypes into an IDE. For example, Maven can be used to generate
Eclipse metadata files:

    mvn clean package
    pushd demo-service
    mvn eclipse:eclipse
    popd

You can then use the Eclipse feature to import existing projects into a
workspace. An alternative is to rely on the
[M2Eclipse](http://www.eclipse.org/m2e/) plugin to import your Maven project
directory contents directly "as a Maven project." Both of these options come
with trade-offs. Which is "better" depends on many factors, including personal
preference. Other IDE's may come with similar features and trade-offs.

However you import a project into your IDE, you should install plugins that will
perform the same code quality checks as when Maven is invoked from the
command-line. For example, in Eclipse, make sure to install *eclipse-cs* and
*FindBugs Eclipse Plugin* from the Eclipse Marketplace. Configure them in the
same way as defined by the *build-tools* module in your generated project, e.g.
by importing the *checkstyle.xml* from the *build-tools* resource directory as
the default checkstyle rules for *eclipse-cs*. If you use an IDE other than
Eclipse, you should find, configure and use similar plugins for checkstyle and
FindBugs.

Finally, note that the generated POM files use
*org.codehaus.mojo:build-helper-maven-plugin* to place unit test and integration
test files in separate subdirectories. This is not necessary when using Maven
directly from the command-line but facilitates the use of IDE's. In particular,
you can configure Eclipse to ignore the contents of the integration tests in the
*src/it* directory when invoking JUnit on a developer's workstation.
